package com.autozone.sdlc.tools.JiraAutomation.Model;

import java.util.List;

public class JiraAddUsers {
    private String keyProject;
    private List users;

    public String getKeyProject() {
        return keyProject;
    }

    public void setKeyProject(String keyProject) {
        this.keyProject = keyProject;
    }

    public List getUsers() {
        return users;
    }

    public void setUsers(List users) {
        this.users = users;
    }
}
