package com.autozone.sdlc.tools.JiraAutomation.Model;

import java.util.List;

public class GetIssues {
    String total;
    private List issues;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List getIssues() {
        return issues;
    }

    public void setIssues(List issues) {
        this.issues = issues;
    }
}
