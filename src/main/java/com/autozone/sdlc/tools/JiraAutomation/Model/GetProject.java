package com.autozone.sdlc.tools.JiraAutomation.Model;

public class GetProject {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
