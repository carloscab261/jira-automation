package com.autozone.sdlc.tools.JiraAutomation.Model;

public class IssueFields {
    private String updated;
    private GetProject  project;

    public GetProject getProject() {
        return project;
    }

    public void setProject(GetProject project) {
        this.project = project;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String update) {
        this.updated = update;
    }
}
