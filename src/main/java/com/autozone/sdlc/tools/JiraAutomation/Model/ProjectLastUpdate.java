package com.autozone.sdlc.tools.JiraAutomation.Model;

import java.util.Comparator;

public class ProjectLastUpdate implements Comparable<ProjectLastUpdate> {
    private String key;
    private String totalIssues;
    private String keyIssue;
    private String update;
    private String name;

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTotalIssues() {
        return totalIssues;
    }

    public void setTotalIssues(String totalIssues) {
        this.totalIssues = totalIssues;
    }

    public String getKeyIssue() {
        return keyIssue;
    }

    public void setKeyIssue(String keyIssue) {
        this.keyIssue = keyIssue;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }


    @Override
    public int compareTo(ProjectLastUpdate o) {
        return this.getUpdate().compareTo(o.getUpdate());

    }
}
