package com.autozone.sdlc.tools.JiraAutomation.Model;

import java.util.List;

public class JiraCreateProject {

    private String key;
    private String description;
    private String lead;
    private String projectTypeKey;
    private String name;
    private String template;
    private List addUsers;

    public List getAddUsers() {
        return addUsers;
    }

    public void setAddUsers(List addUsers) {
        this.addUsers = addUsers;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLead() {
        return lead;
    }

    public void setLead(String lead) {
        this.lead = lead;
    }

    public String getProjectTypeKey() {
        return projectTypeKey;
    }

    public void setProjectTypeKey(String projectTypeKey) {
        this.projectTypeKey = projectTypeKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
